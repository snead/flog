/******* REQUIRE *******/
var daemon = require('daemon')();
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

/* bash $ export flog_port=4444 */
var port = process.env.flog_port || 3000;
http.listen(port, function(){
    console.log('listening on *:'+port);
});

/******* STORAGE *******/
var storage = require('node-persist');
storage.initSync();

/******* SOCKET *******/
io.on('connection', function(socket){

    console.log('New connection established');

    /******* SET LOG *******/
    socket.on('set', function(msg){
        storage.setItem(msg.key, msg.value, function(err){
            if(err) {
                io.emit('message', err);
            } else {
                io.emit('message', msg);
            }
        });
    });

    /******* GET LOG *******/
    socket.on('get', function(msg){
        storage.getItem(msg.key, function(err, value){
            io.emit('logMessage', '<h1>' + msg.key + '</h1><pre>' + value + '</pre>');
        });
    });

    socket.on('disconnect', function() {
        console.log('Client disconnected.');
    });
});

/* SEND INDEX.HTML TO FRONTEND */
function sendIndexHTML(req, res){
    res.sendFile(__dirname + '/index.html');
}
/*<script src="./node_modules/jqlite/jqlite.min.js"></script>
<script src="./node_modules/socket.io/lib/socket.js"></script>*/
app.use('/scripts/jqlite.js', express.static(__dirname + '/node_modules/jqlite/jqlite.min.js'));
app.use('/scripts/socket.js', express.static(__dirname + '/node_modules/socket.io/node_modules/socket.io-client/socket.io.js'));
app.get('/', sendIndexHTML);
app.post('/', sendIndexHTML);